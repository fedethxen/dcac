import './App.css';
import Productos from './componentes/Productos'


function App() {
  return (
    <div className="App">
      <Productos />
    </div>
  );
}

export default App;

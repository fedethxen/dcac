import React from 'react'
import Producto from './Producto'
import axios from 'axios'
import './Productos.css'

const URL_API = 'http://localhost:3080/api/productos/'

class Productos extends React.Component {

    state = {
        productos: []
    }

    async componentDidMount() {
        try {
            let {data:dataProductos} = await axios(URL_API)
            console.log(dataProductos.productos)
            this.setState({productos: dataProductos.productos})
        }
        catch(error) {
            console.error('Error en axios componentDidMount', error)
        }
    }

    render() {
        let { productos } = this.state
        return (
            <div className="Productos">
                <div className="container mt-3">
                    <div className="jumbotron">
                        {/*<a href="" className="btn-agregar">NUEVO +</a>*/}
                        <h2>Productos</h2>                      
                        <br />
                        {
                           productos.map( producto => 
                                <Producto 
                                    producto={producto}
                                    key={producto.id}
                                />
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default Productos
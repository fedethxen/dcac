import './Producto.css'
import imagen from './images/productos.png'
import axios from 'axios'
import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

const URL_API = 'http://localhost:3080/api/update-producto/'

function Producto(props) {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [form, setForm] = useState({
        id: props.producto.id,
        nombre: "",
        precio: "",
        descripcion: "",
    });

    const onUpdateField = e => {
        const nextFormState = {
            ...form,
            [e.target.name]: e.target.value,
        };
        //console.log(e.target.value)
        setForm(nextFormState);
    };

    const onSubmitForm = e => {
        e.preventDefault();
        //validar datos antes de eviar

        axios.post(URL_API,form)
        .then(res=>{
            if(res.data.status==='success'){              
                alert(res.data.message);
                window.location.reload()
            }else{
                alert(res.data.message);
            }
            console.log(res.data)
        })  
        
    };

    let { producto } = props
    return (
    <>
        <div className="Producto">
            <div className="media alert alert-primary" onClick={handleShow} >
                <img src={imagen} className="img-product mr-3" alt={producto.id} />
                <div className="media-body">
                    <p><u>ID:</u> {producto.id}</p>
                    <p><b>{producto.nombre}</b></p>
                    <p>{producto.descripcion}</p>
                    <p><b>Precio: </b>$ {producto.precio}</p>
                    <p><b>Precio (usd): </b>$ {producto.precio_dolar}</p>
                </div>
            </div>
        </div>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header >
          <Modal.Title>Editar Producto: {producto.id}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={onSubmitForm}>
            <Form.Group className="mb-3" >
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder={producto.nombre}
                autoFocus
                name="nombre"
                value={form.nombre}
                onChange={onUpdateField}
              />
            </Form.Group>
            <Form.Group className="mb-3" >
              <Form.Label>Precio</Form.Label>
              <Form.Control
                type="number"
                placeholder={producto.precio}
                name="precio"
                value={form.precio}
                onChange={onUpdateField}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Descripcion</Form.Label>
              <Form.Control 
                as="textarea" 
                rows={3}
                placeholder={producto.descripcion}
                name="descripcion"
                value={form.descripcion}
                onChange={onUpdateField}
                />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={onSubmitForm}>
            Actualizar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
    )
}

export default Producto
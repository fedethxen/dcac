## DESPLIEGUE DEL ENTORNO

* Requisitos:

    - Docker
    - Puertos 3000 y 3080 liberados

* Tecnologias:

    BACKEND: Laravel 9

    FRONTEND: React 18

* Ejecucion:

Posicionarse en el directorio raiz, donde se encuentra el `docker-compose.yml`, y ejecutar los siguientes comandos:

1) Crear imagenes y correr los containers: 

```    
docker-compose up -d --build
```
2) Instalamos dependencias composer:

``` 
docker-compose exec app composer install
``` 

3) Crear tablas con migrate:

``` 
docker-compose exec app php artisan migrate
``` 
4) Cargar las tablas mediante seeders:

``` 
docker-compose exec app php artisan db:seed RellenarProductos
``` 
``` 
docker-compose exec app php artisan db:seed CargaDolar
``` 

# Uso/Testing

- Backend 

    Utilizando herramientas como Postman por ejemplo se espera un json con formato (.ejmp):

    - CREAR
    ``` 
    {
        "id": "21",
        "nombre":"Teclado",
        "descripcion":"Genius",
        "precio":"6500"
    }
    ``` 

    - ACTUALIZAR
    ``` 
    {
        "nombre":"Teclado",
        "descripcion":"Genius",
        "precio":"6500"
    }
    ``` 

- Metodo HTTP y URLs:

    - CREATE:

        POST http://localhost:3080/api/productos/

    - READ:

        GET http://localhost:3080/api/productos/  
        GET http://localhost:3080/api/productos/{id} 

    - UPDATE:

        POST http://localhost:3080/api/update-producto/ 

    - DELETE:

        DELETE http://localhost:3080/api/delete-producto/{id}

- Frontend

    Ir a http://localhost:3000 

    Se visualiza el listado de los productos y haciendo click en cualquier producto se pueden actualizar los datos corrrespondientes



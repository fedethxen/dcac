<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\Dolar;

use Illuminate\Support\Facades\Validator;

class ApiProductosController extends Controller
{
    public function getProductos(){

        $productos= Productos::select('id','nombre','descripcion','precio')->orderBy('id', 'DESC')->get();
        $dolar= Dolar::where('tipo', 'blue')->get();

        if(isset($productos)){    
            foreach ($productos as $producto) {
                $dataProductos[]=[
                    'id' => $producto->id,
                    'nombre' => $producto->nombre,
                    'descripcion' => $producto->descripcion,
                    'precio' => $producto->precio,
                    'precio_dolar'=> round(($producto->precio)/($dolar[0]->precio),2)
                ];
            }  
            $data = array(
                'productos'=>$dataProductos,
                'status'=>'success',
                'code'=>200
            );
        }else{
            $data = array(
                'message'=>	'No existen productos',
                'status' =>	'error',
                'code'=>400
            ); 
        }
        return response()->json($data,200);
    }

    public function getProducto($id){

        $producto= Productos::find($id);
        $dolar= Dolar::select('precio')->where('tipo', 'blue')->get();

        if(isset($producto)){
            $dataProducto[]=[
                'id' => $producto->id,
                'nombre' => $producto->nombre,
                'descripcion' => $producto->descripcion,
                'precio' => $producto->precio,
                'precio_dolar'=> round(($producto->precio)/($dolar[0]->precio),2)
            ];
            $data=array(
                'producto'=>$dataProducto,
                'status'=>'success',
                'code'=>200
            );
        }else{
            $data = array(
                'message'=>'No existe el producto solicitado',
                'status' =>	'error',
                'code'=>400
            );
        }
        return response()->json($data,200);
    }

    public function setProducto(Request $request){
      
        $data=$request->all();

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|min:5|max:35',
            'precio' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message){
                $data=array(
                    'message'=>$message,
                    'status'=>'error',
                    'code'=>400
                );
                return response()->json($data,200);
            }
        }

        $producto= new Productos();
        $producto->nombre = $data['nombre'];
        $producto->descripcion = isset($data['descripcion']) ? $data['descripcion'] : null  ;
        $producto->precio = $data['precio'];
        $producto->save();

        $data=array(
            'message'=>'Producto CREADO',
            'status'=>'success',
            'code'=>200
        );
        return response()->json($data,200);

    } 

    public function updateProducto(Request $request){

        $data=$request->all();

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|min:5|max:80',
            'precio' => 'required|numeric',
        ]);
 
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message){
                $data=array(
                    'message'=>$message,
                    'status'=>'error',
                    'code'=>400
                );
                return response()->json($data,200);
            }
        }

        $producto= Productos::FindOrFail($data['id']);
        $producto->nombre = $data['nombre'];
        $producto->descripcion = isset($data['descripcion']) ? $data['descripcion'] : null  ;
        $producto->precio = $data['precio'];
        $producto->save();

        $data=array(
            'message'=>'Producto ACTUALIZADO',
            'status'=>'success',
            'code'=>200
        );
        return response()->json($data,200);

    } 

    public function deleteProducto($id){

        $producto= Productos::find($id);
        $nombre= $producto->nombre;
        $producto->delete();

        $data=array(
            'message'=>'Producto: '.$nombre.' ELIMINADO',
            'status'=>'success',
            'code'=>200
        );
        return response()->json($data,200);
    }
}

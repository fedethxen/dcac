## About Laravel

* Historial de comandos:

    1  php artisan
    2  php artisan migrate:status
    3  php artisan migrate
    4  php artisan
    5  php artisan make:seeder rellenar-producto
    6  php artisan db:seed RellenarProductos
    7  php artisan db:seed RellenarProductos
    8  php artisan db:seed --class=RellenarProductos
    9  php artisan db:seed --class=RellenarProductos
   10  php artisan db:seed --class=RellenarProductos
   11  php artisan db:seed RellenarProductos
   12  php artisan db:seed RellenarProductos
   13  php artisan make:model Productos
   14  php artisan make:controller ApiProductosController
   15  php artisan route:list
   16  chmod -R 775 storage
   17  chmod -R 775 bootstrap/cache
   18  ls -al
   19  chmod -R 777 storage
   20  chmod -R 777 bootstrap/cache
   25  php artisan migrate help
   26  php artisan
   27  php artisan migrate:refresh
   28  php artisan db:seed RellenarProductos
   29  php artisan make -h
   30  php artisan make:migration -h
   31  php artisan make:migration create_dolar_table
   32  php artisan make:seeder CargaDolar
   33  php artisan migrate
   34  php artisan db:seed CargaDolar
   35  php artisan db:seed CargaDolar
   36  php artisan db:seed CargaDolar
   37  php artisan make:model Dolar    

* Entrar a los contenedores

    - App:
        docker-compose exec app bash

    - react-app:
        (docker ps -a)
        docker exec -it [id_contenedor_react_app] sh



* Anotaciones:

- Voy a tener que crear script bash en contenedor de app y mysql llamar el script como 'command' desde el .yml
- mysql: va a tener que crear la db vacia
- ejecutar los script necesarios de laravel y los del history que fueron necesarios

# Comados Docker:

Detener contenedores eliminar imagines y borrar la DB:
```
docker-compose down --remove-orphans -v
```

# DEPLOY:

docker-compose up -d --build
docker-compose exec app php artisan migrate
docker-compose exec app php artisan db:seed RellenarProductos
docker-compose exec app php artisan db:seed CargaDolar
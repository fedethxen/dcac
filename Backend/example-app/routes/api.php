<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiProductosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Rutas API - CRUD Productos
//Route::prefix('dcac')->group(function () {}
Route::get('/productos', [ApiProductosController::class, 'getProductos']);
Route::get('/productos/{id}', [ApiProductosController::class, 'getProducto']);
Route::post('/productos', [ApiProductosController::class, 'setProducto']);
Route::post('/update-producto', [ApiProductosController::class, 'updateProducto']);
Route::delete('/delete-producto/{id}', [ApiProductosController::class, 'deleteProducto']);




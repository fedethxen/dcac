<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use DB;

class RellenarProductos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 20 ; $i++) { 
            $x=rand(1000,50000);
            DB::table('productos')->insert(array(
                'nombre'=>'Producto '.$i,
                'descripcion'=>'Descripcion del producto '.$i,
                'precio'=> $x,
                'created_at'=> date("Y-m-d H:i:s"),
            ));
        }   
        $this->command->info('Tabla productos cargada correctamente');
    }
}

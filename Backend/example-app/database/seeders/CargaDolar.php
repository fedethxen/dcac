<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use DB;

class CargaDolar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dolar')->insert(array(
            'tipo'=>'oficial',
            'precio'=> 140.78,
            'created_at'=> date("Y-m-d H:i:s"),
        ));
        DB::table('dolar')->insert(array(
            'tipo'=>'blue',
            'precio'=> 293,
            'created_at'=> date("Y-m-d H:i:s"),
        ));
         
        $this->command->info('Tabla dolar cargada correctamente');
    }
}
